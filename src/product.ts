import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new Product into the database...")
    const productRepository = AppDataSource.manager.getRepository(Product)

    console.log("Loading Products from the database...")
    const products = await productRepository.find()
    console.log("Loaded Products: ", products)

    const updateProduct = await productRepository.findOneBy({id:1})
    console.log(updateProduct)
    updateProduct.price = 120
    await productRepository.save(updateProduct)


}).catch(error => console.log(error))
